#!/usr/bin/env python
# coding: utf-8

# In[1]:


import glob
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster


# In[2]:


def read(group):
    files = glob.glob(group+"/*.png")
    x = []
    count = 0
    for i in files:
        img = np.array(plt.imread(i)).astype(float)
        x.append(img.flatten())
    return files, np.array(x)

def slope(x,y):
    m = []
    for i in range(len(x)-1):
        m.append( (y[i+1] - y[i])/(x[i+1] - x[i]) )
    return m


# In[3]:


names, X = read("imagenes")
N = len(names)


# In[4]:


n_clusters = np.arange(1,20,1)
Inertia = []

for i in n_clusters:
    k_means = sklearn.cluster.KMeans(n_clusters=i)
    k_means.fit(X)
    Inertia.append(k_means.inertia_)


# In[8]:


fig = plt.figure(figsize=(6,6))
plt.plot(n_clusters,Inertia)
plt.ylabel("Inercia",size=15)
plt.xlabel("Número de Clústers",size=15)
plt.grid()
plt.tight_layout()
plt.savefig("inercia.png",bbox_inches='tight')
plt.show()


# In[6]:


# m = slope(n_clusters,Inertia)
# plt.plot(n_clusters[:-1],m)
# plt.ylabel("Pendiente")
# plt.xlabel("Número de Clústers")
# plt.grid()


# In[7]:


n_cluster = 4 #Seleccionando el punto donde más cambia

k_means = sklearn.cluster.KMeans(n_clusters=n_cluster)
k_means.fit(X)
cluster = k_means.predict(X)

X_n = []
Dist = []
K = []

for i in range(n_cluster):
    X_temp = X[cluster == i]
    for j in X_temp:
        X_n.append(j)
        Dist.append(np.linalg.norm(j+k_means.cluster_centers_[i]))
        K.append(i)

fig = plt.figure(figsize=(25,20)) 
h = 1
for i in range(n_cluster):
    ii = (np.array(K) == i)
    jj = np.argsort(Dist)[ii][:5]
    for k, j in enumerate(jj):
        plt.subplot(4,5,h)
        plt.title("k ="+str(i),size=20)
        plt.imshow(X_n[j].reshape(100,100,3))
        h += 1

plt.tight_layout()
plt.savefig("ejemplo_clases.png",bbox_inches='tight')        
plt.show()

