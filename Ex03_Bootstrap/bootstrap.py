#!/usr/bin/env python
# coding: utf-8

# In[9]:


import numpy as np
import matplotlib.pyplot as plt

import sklearn.model_selection
import sklearn.linear_model

get_ipython().run_line_magic('matplotlib', 'inline')


# In[3]:


data = np.loadtxt("notas_andes.dat", skiprows=1)


# In[14]:


X = data[:,:4]
Y = data[:,4]
N = len(Y)


# In[55]:


M = 1000
betas = []
regresion = sklearn.linear_model.LinearRegression()
for i in range(M):
    Id = np.random.choice(np.arange(N),N)
    regresion.fit(X[Id], Y[Id])
    betas.append(regresion.coef_)


# In[62]:


fig = plt.figure(figsize=(10,10))

for i in range(4):
    plt.subplot(2,2,i+1)
    plt.hist(np.array(betas)[:,i])
    beta_av = np.average(np.array(betas)[:,i])
    beta_std = np.std(np.array(betas)[:,i])
    plt.title(r'$\beta_'+str(i+1)+' = '+str(round(beta_av,2))+'  \pm '+str(round(beta_std,2))+' $')
plt.savefig("bootstrap.png")
plt.show()


# In[ ]:




