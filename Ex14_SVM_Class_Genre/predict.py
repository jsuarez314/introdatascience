#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import glob
import matplotlib.pyplot as plt

from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix

#Mujer 0
#Hombre 1

def read(group):
    files = glob.glob(group+"/*.jpg")
    x = []
    y = []
    count = 0
    for i in files:
        img = np.array(plt.imread(i)).astype(float)
        x.append(img.flatten())
        if group == "train":
            label = (int(i[6:-4])+1)%2
            y.append(label)
    return files, np.array(x), np.array(y)


# In[2]:


files_train, x , y = read("train")
files_valid, x_valid, y_valid = read("test")


# In[3]:


scaler = StandardScaler()
x_train, x_test, y_train, y_test = train_test_split(x, y, train_size=0.7)

x_train = scaler.fit_transform(x_train)
x_test = scaler.transform(x_test)
x_valid = scaler.transform(x_valid)


# In[4]:


# cov = np.cov(x_train[y_train==cond].T)
# valores, vectores = np.linalg.eig(cov)
# valores = np.real(valores)
# vectores = np.real(vectores)
# ii = np.argsort(-valores)
# valores = valores[ii]
# vectores = vectores[:,ii]
# X_train = x_train@vectores.T
# X_test = x_test@vectores.T
# clf = LinearDiscriminantAnalysis()
# nc=10
# Xx_train = X_train[:,:nc]
# Xx_test = X_test[:,:nc]
# clf.fit(Xx_train, y_train)
# y_pred = clf.predict(Xx_test)


# In[5]:


C = np.logspace(-3,5,100)
F1 = []

ii = 0
for i in C:
    clf = SVC(C=i, kernel='linear')
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)
    F1.append(f1_score(y_test, y_pred))


# In[6]:


C_max= C[np.argmax(F1)]


# In[7]:


clf = SVC(C=C_max, kernel='linear')
clf.fit(x_train, y_train)
y_pred = clf.predict(x_valid)

out = open("test/predict_test.csv","w")
out.write("Name,Target\n")
for i,j in zip(files_valid,y_pred):
    out.write("{},{}\n".format(i.split("/")[-1],j))
#     print(i,j)
out.close()

