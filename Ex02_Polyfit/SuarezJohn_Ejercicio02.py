#!/usr/bin/env python
# coding: utf-8

# In[47]:


import numpy as np
import matplotlib.pyplot as plt


# In[21]:


data = np.loadtxt("numeros_20.txt")

x = data.T[0]
y = data.T[1]

x_train = x[:10]
y_train = y[:10]
x_test = x[10:]
y_test = y[10:]


# In[61]:


class PolyFit:

    def __init__(self, degree = 0):
        self.degree = degree
        self.beta =  np.zeros(degree + 1)

    def fit(self,x,y):
        m = self.degree + 1
        n = len(x)
        X = np.zeros((n,m))
        Y = np.zeros(n)
        for i in range(0,n):
            for j in range(0,m):
                X[i][j] = x[i]**j
                Y[i] = y[i]
        X_inv = np.linalg.pinv(X)
        self.beta = X_inv@Y
        
    def predict(self,x):
        y = np.zeros(x.shape)
        m = self.degree + 1
        for i in range(m):
            for j in range(len(x)):
                y[j] += self.beta[i]*x[j]**i    
        return y

    def score(self,x,t):
        m = self.degree + 1
        N = len(x)
        y = np.zeros(N)
        err = 0
        for i in range(m):    
            for j in range(N):
                y[j] += self.beta[i]*x[j]**i

        for j in range(N):
            err += 0.5*(y[j]-t[j])**2
        return np.sqrt(2*err/N)


# In[63]:


m = np.linspace(0,11,12).astype(int)

Err_train = []
Err_test = []

fig = plt.figure(figsize=(10,10))

for i in m:
    A = PolyFit(degree = i)
    A.fit(x_train,y_train)
    
    x_plot=np.linspace(0,1,100)
    y_plot=A.predict(x_plot)
    
    plt.subplot(4,3,i+1)
    plt.title("M="+str(i))
    plt.plot(x_plot,y_plot)
    plt.plot(x_train,y_train,"o")
    plt.ylim(0,2)
    plt.xlim(0,1)
    
    err_train = A.score(x_train,y_train)
    err_test = A.score(x_test,y_test)    
    Err_train.append(err_train)
    Err_test.append(err_test)
    
    
plt.tight_layout()
plt.savefig("14.png")
plt.show()
    
plt.semilogy(m,Err_train,"o-",label="Train")
plt.semilogy(m,Err_test,"o-", label="Test")
plt.grid()
plt.legend()
plt.savefig("15.png")
plt.show()

