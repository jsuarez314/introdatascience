#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt


# In[2]:


data = np.loadtxt("numeros_20.txt")

x = data.T[0]
y = data.T[1]

x_train = x[:10]
y_train = y[:10]
x_test = x[10:]
y_test = y[10:]


# In[3]:


fig = plt.figure(figsize=(12,4))

plt.subplot(1,3,1)
plt.plot(x,y,"o")
plt.xlabel("x")
plt.ylabel("y")
plt.title("ALL")

plt.subplot(1,3,2)
plt.plot(x_train,y_train,"o",c="r")
plt.plot(x,y,"o",alpha=0.2)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Train")

plt.subplot(1,3,3)
plt.plot(x_test,y_test,"o",c="b")
plt.plot(x,y,"o",alpha=0.2)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Test")
plt.tight_layout()
plt.show()


# In[4]:


def fit(m,x,y):
    m = m + 1
    n = len(x)
    X = np.zeros((n,m))
    Y = np.zeros(n)
    for i in range(0,n):
        for j in range(0,m):
            X[i][j] = x[i]**j
            Y[i] = y[i]
#     X_inv = np.linalg.inv(X) 
#     c = X_inv@Y
#     c = (np.linalg.inv(X.T@X))@(X.T@Y)
    X_inv = np.linalg.pinv(X)
    c = X_inv@Y
    return x,y,c

def poly(m,c):
    m = m + 1
    x = np.linspace(0,1,100)
    y = np.zeros(x.shape)
    for i in range(m):
        for j in range(len(x)):
            y[j] += c[i]*x[j]**i    
    return x, y

def error(x,m):
    for i in range(len(x)):
        print(i) 
    return 0.5*(y-t)**2


# In[5]:


fig = plt.figure(figsize=(10,10))

m = np.linspace(0,11,12).astype(int)
for i in m:
    xx, yy, c = fit(i,x_train,y_train)
    xp, yp = poly(i,c)
    
    plt.subplot(4,3,i+1)
    plt.ylim(0,2)
    plt.title("M = "+str(i))
    plt.plot(x_train, y_train,"o",c="r")
    plt.plot(xp, yp)
    plt.xlabel("x")
    plt.ylabel("y")
    
plt.tight_layout()
plt.savefig("14_fig.png")
plt.show()


# In[6]:


def E(m,c,x,t):
    m = m + 1
    N = len(x)
    y = np.zeros(N)
    err = 0
    for i in range(m):    
        for j in range(N):
            y[j] += c[i]*x[j]**i
    
    for j in range(N):
        err += 0.5*(y[j]-t[j])**2
    return np.sqrt(2*err/N)


# In[8]:


Err_train = []
Err_test = []

fig = plt.figure(figsize=(15,15))
m = np.linspace(0,11,12).astype(int)

for i in m:
    xx, yy, c = fit(i,x_train,y_train)
    xp, yp = poly(i,c)
    
    plt.subplot(4,3,i+1)
    plt.ylim(0,2)
    plt.title("M = "+str(i),size=20)
    plt.plot(x_test, y_test,"o",c="b",label="Test")
    plt.plot(xp, yp)
    plt.xlabel("x",size=20)
    plt.ylabel("y",size=20)
    plt.plot(x_train, y_train,"o",alpha=0.2,c="r",label="Train")
    plt.plot(xp, yp)   
    
    err_train = E(i,c,x_train,y_train)
    err_test = E(i,c,x_test,y_test)
    Err_train.append(err_train)
    Err_test.append(err_test)

    plt.legend()    
    
plt.tight_layout()
plt.savefig("14_fig.png")
plt.show()

fig = plt.figure(figsize=(15,5))

plt.subplot(1,3,1)
plt.plot(m,Err_train,"o-",c="r",label="Train")
plt.plot(m,Err_test,"o-",c="b",label="Test")
plt.legend()
plt.ylabel("ERMS",size=20)
plt.xlabel("M",size=20)
plt.ylim(0,2)
plt.xlim(0,5)

plt.subplot(1,3,2)
plt.plot(m,Err_train,"o-",c="r",label="Train")
plt.plot(m,Err_test,"o-",c="b",label="Test")
plt.legend()
plt.ylabel("ERMS",size=20)
plt.xlabel("M",size=20)
plt.ylim(0,20)
plt.xlim(0,6)

plt.subplot(1,3,3)
plt.plot(m,Err_train,"o-",c="r",label="Train")
plt.plot(m,Err_test,"o-",c="b",label="Test")
plt.legend()
plt.ylabel("ERMS",size=20)
plt.xlabel("M",size=20)

plt.tight_layout()
plt.savefig("15_fig.png")
plt.show()

plt.semilogy(m,Err_train,"o-",c="r",label="Train")
plt.semilogy(m,Err_test,"o-",c="b",label="Test")
plt.legend()
plt.ylabel("ERMS",size=20)
plt.xlabel("M",size=20)

plt.tight_layout()
plt.grid()
plt.savefig("15log_fig.png")
plt.show()

